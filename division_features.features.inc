<?php
/**
 * @file
 * division_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function division_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function division_features_image_default_styles() {
  $styles = array();

  // Exported image style: main-image.
  $styles['main-image'] = array(
    'name' => 'main-image',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1024,
          'height' => 200,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'main-image',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function division_features_node_info() {
  $items = array(
    'landing_page' => array(
      'name' => t('Landing Page'),
      'base' => 'node_content',
      'description' => t('Use <em>landing pages</em> at the start of each section. You can attach blocks to this page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic Page'),
      'base' => 'node_content',
      'description' => t('Use this content type to create a simple page with static content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'person' => array(
      'name' => t('Person'),
      'base' => 'node_content',
      'description' => t('Add a <em>person</em> using information from the <a href="https://courses.hfcc.edu/staff-directory" target="_blank">HFCC Faculty and Staff Directory</a> for use in this site\'s faculty or staff lists.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
