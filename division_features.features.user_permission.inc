<?php
/**
 * @file
 * division_features.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function division_features_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access bean overview'.
  $permissions['access bean overview'] = array(
    'name' => 'access bean overview',
    'roles' => array(
      'administrator' => 'administrator',
      'supereditor' => 'supereditor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'administer bean settings'.
  $permissions['administer bean settings'] = array(
    'name' => 'administer bean settings',
    'roles' => array(
      'administrator' => 'administrator',
      'supereditor' => 'supereditor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'administer bean types'.
  $permissions['administer bean types'] = array(
    'name' => 'administer bean types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'administer beans'.
  $permissions['administer beans'] = array(
    'name' => 'administer beans',
    'roles' => array(
      'administrator' => 'administrator',
      'supereditor' => 'supereditor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'create any anywhere_button bean'.
  $permissions['create any anywhere_button bean'] = array(
    'name' => 'create any anywhere_button bean',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'create any freeform_block bean'.
  $permissions['create any freeform_block bean'] = array(
    'name' => 'create any freeform_block bean',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'create any heading bean'.
  $permissions['create any heading bean'] = array(
    'name' => 'create any heading bean',
    'roles' => array(
      'administrator' => 'administrator',
      'supereditor' => 'supereditor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'delete any anywhere_button bean'.
  $permissions['delete any anywhere_button bean'] = array(
    'name' => 'delete any anywhere_button bean',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'delete any freeform_block bean'.
  $permissions['delete any freeform_block bean'] = array(
    'name' => 'delete any freeform_block bean',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'delete any heading bean'.
  $permissions['delete any heading bean'] = array(
    'name' => 'delete any heading bean',
    'roles' => array(
      'administrator' => 'administrator',
      'supereditor' => 'supereditor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'edit any anywhere_button bean'.
  $permissions['edit any anywhere_button bean'] = array(
    'name' => 'edit any anywhere_button bean',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'edit any freeform_block bean'.
  $permissions['edit any freeform_block bean'] = array(
    'name' => 'edit any freeform_block bean',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'edit any heading bean'.
  $permissions['edit any heading bean'] = array(
    'name' => 'edit any heading bean',
    'roles' => array(
      'administrator' => 'administrator',
      'supereditor' => 'supereditor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'edit bean view mode'.
  $permissions['edit bean view mode'] = array(
    'name' => 'edit bean view mode',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view any anywhere_button bean'.
  $permissions['view any anywhere_button bean'] = array(
    'name' => 'view any anywhere_button bean',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view any freeform_block bean'.
  $permissions['view any freeform_block bean'] = array(
    'name' => 'view any freeform_block bean',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view any heading bean'.
  $permissions['view any heading bean'] = array(
    'name' => 'view any heading bean',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'supereditor' => 'supereditor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view bean page'.
  $permissions['view bean page'] = array(
    'name' => 'view bean page',
    'roles' => array(
      'administrator' => 'administrator',
      'supereditor' => 'supereditor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view bean revisions'.
  $permissions['view bean revisions'] = array(
    'name' => 'view bean revisions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  return $permissions;
}
