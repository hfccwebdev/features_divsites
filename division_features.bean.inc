<?php
/**
 * @file
 * division_features.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function division_features_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'anywhere_button';
  $bean_type->label = 'Anywhere Button';
  $bean_type->options = '';
  $bean_type->description = 'Add a navigation button that will work anywhere on the site.';
  $export['anywhere_button'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'freeform_block';
  $bean_type->label = 'Freeform Block';
  $bean_type->options = '';
  $bean_type->description = 'Create an unformatted block that can go anywhere.';
  $export['freeform_block'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'heading';
  $bean_type->label = 'Heading';
  $bean_type->options = '';
  $bean_type->description = 'Create a block heading to go in the Billboard region.';
  $export['heading'] = $bean_type;

  return $export;
}
